# Q-Fetcher React Application
## Q-Fetcher project UI of the home assignment for the interview in Chegg company

Provides the UI for the Q-Fetcher back-end      
Deployed to [https://q-fetcher.mizrahi.co/](https://q-fetcher.mizrahi.co/)      

# Getting started

## Prerequisites

* Node & npm

## Building for prod

```
cd qfetcher-react
npm run build
```

## Local run

```
npm start
```

## Local tests run
(not implemented yet)

```
npm test
```

# Configuration

The following environment variables may or need to be specified to run the application:

* `REACT_APP_MANIFEST_URI` -- link to the manifest URI which contains list of the source links
* `REACT_APP_BACKEND_URI` -- back-end application URI

# Project structure

```
├── .env
├── .gitignore
├── package.json
├── package-lock.json
├── README.md
└── src
    ├── App.css
    ├── App.js
    ├── App.test.js
    ├── components
    │         ├── checkbox
    │         │         ├── CheckBox.js
    │         │         └── index.js
    │         └── question
    │             ├── index.js
    │             ├── Question.css
    │             └── Question.js
    ├── index.css
    ├── index.js
    ├── serviceWorker.js
    └── setupTests.js

```
import React, { Component, createRef} from 'react'
import './App.css'
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

import CheckBox from './components/checkbox/CheckBox'
import Question from './components/question/Question'

require('dotenv').config()

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      extensions: [
        {id: 1, value: "jpg", isChecked: false},
        {id: 2, value: "png", isChecked: false},
        {id: 3, value: "bmp", isChecked: false},
        {id: 4, value: "tiff", isChecked: false},
        {id: 1, value: "pdf", isChecked: false},
        {id: 2, value: "csv", isChecked: false},
        {id: 3, value: "json", isChecked: false},
      ],
      questions: [],
      masterSelected: Boolean,
      isFetching: false
    }
    this.checkboxRef = createRef();
  }

  componentDidMount() {
    if(this.checkboxRef.current) {
      this.checkboxRef.current.indeterminate = this.state.indeterminate;
    }
  }
  
  handleAllChecked = (event) => {
    let extensions = this.state.extensions
    extensions.forEach(extension => extension.isChecked = event.target.checked) 
    this.setState({extensions: extensions})
  }

  handleCheckChieldElement = (event) => {
    let extensions = this.state.extensions
    extensions.forEach(extension => {
       if (extension.value === event.target.value)
          extension.isChecked = event.target.checked
    })
    this.setState({extensions: extensions})
  }

  sendFormDataToBackEnd = (event) => {
    let extensions = this.state.extensions
    const backEndUri = process.env.REACT_APP_BACKEND_URI
    const manifestUri = process.env.REACT_APP_MANIFEST_URI
    let filter = extensions.filter(e => e.isChecked === true).map(e => e.value)
    let data = {
      manifest: manifestUri,
      filter: filter
    }

    this.setState({isFetching: true, questions: []})

    fetch(backEndUri + '/api/v1/fetch', {
        method: 'post',
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(data)
    })
    .then(this.handleErrors)
    .then((data) => {
        this.setState({ questions: data.questions});
    })
  }

  handleErrors = (response) => {

    this.setState({isFetching: false})

    if (!response.ok) {
      return {
        questions: [{source: 'Error'}]
      }
    } else {
      return response.json()
    }
  }

  render() {
    return (
      <div className="App">
        <header className="main-header">
          <div className="main-header-content">
              <h1>Q-Fetcher</h1>
          </div>
        </header>
        <section className="wrap-whole">
          <section className="vertical-nav-bar">
              <div className="menu-container">
                <div className="main-nav">
                  <h2>Source filter</h2>
              <form onSubmit={(e) => {this.sendFormDataToBackEnd(); e.preventDefault();}}>
                  <ul>
                  {
                    this.state.extensions.map((extension, index) => {
                      return (<CheckBox key={index} handleCheckChieldElement={this.handleCheckChieldElement}  {...extension} />)
                    })
                  }
                  </ul>
                <input type="submit" value="Send" className="Submit"/>
              </form>
                </div>
              </div>
              <div className="menu-container">
              </div>
          </section>
          <section className="container__main">
              <header className="container__header">
              <h2>Questions</h2>
              </header>


              <article className="container__content">
                  {this.state.isFetching ?
                      (<div className="div-spinner">
                          <Loader type="TailSpin" color="#00BFFF" height="40" width="40" />
                      </div>) : null
                      }
                {this.state.questions.map((question, i) => {
                    return (<Question className="Question" key={i} {...question}/>)
                })}
              </article>
          </section>
        </section>
      </div>
    );
  }
}

export default App
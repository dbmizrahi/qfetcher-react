import React from 'react'
import "./Question.css"

export const Question = (question, i) => {
    return (
        <div className="Question" key={i}>
            <p><b>Question text:</b> {question.value}</p><p><b className="Source">Source:</b> {question.source}</p>
        </div>
    )
}

export default Question